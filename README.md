# ispy modified (again)

## Video
[![ispy modified](https://i.ytimg.com/vi/I3ILtuoBqGg/hqdefault.jpg)](https://gitlab.com/VoXaN24/ispy/-/raw/main/screenshot/video.mp4)

<p align="center"><img src="https://gitlab.com/VoXaN24/ispy/-/raw/main/screenshot/1.jpg"></p>
<h4 align="center">
ispy : Eternalblue(ms17-010)/Bluekeep(CVE-2019-0708) Scanner and exploiter ( Metasploit automation )
</h4>

# How to install :
- Kali linux / Parrot OS :
```
git clone https://gitlab.com/VoXaN24/ispy.git
cd ispy
chmod +x setup.sh
./setup.sh
```
-Debian :
```
git clone https://gitlab.com/VoXaN24/ispy.git
cd ispy
chmod +x setup-deb.sh
./setup-deb.sh
```
# Screenshots :
![Image 2](https://gitlab.com/VoXaN24/ispy/-/raw/main/screenshot/2.jpg)
![Image 3](https://gitlab.com/VoXaN24/ispy/-/raw/main/screenshot/3.jpg)
![Image 4](https://gitlab.com/VoXaN24/ispy/-/raw/main/screenshot/4.jpg)
![Image 5](https://gitlab.com/VoXaN24/ispy/-/raw/main/screenshot/5.jpg)
![Image 6](https://gitlab.com/VoXaN24/ispy/-/raw/main/screenshot/6.jpg)

# Tested On :
* Parrot OS 
* Kali linux
* Debian 10/11
# Tutorial ( How to use ispy )
* https://gitlab.com/VoXaN24/ispy/-/raw/main/screenshot/video.mp4
# info
* GitHub Author profile : https://github.com/Cyb0r9
* E-mail address : admin@voxhost.fr

# Disclaimer :
<br><b>usage of ispy for attacking targets without prior mutual consent is illegal.</b></br>
<b>ispy is for security testing purposes only</b>

# Original depot :
[lien](https://github.com/getdrive/ispy)