#!/bin/bash
#Colors
white="\033[1;37m"
grey="\033[0;37m"
purple="\033[0;35m"
red="\033[1;31m"
green="\033[1;32m"
yellow="\033[1;33m"
Cyan="\033[0;36m"
Cafe="\033[0;33m"
Fiuscha="\033[0;35m"
blue="\033[1;34m"
nc="\e[0m"
#
path=/opt/metasploit-framework/embedded/framework/modules/exploits/windows/smb/eternalblue_doublepulsar.rb # new Path for MSF6
clear
if hash msfconsole 2>/dev/null; then
echo -e "$white[$green+$white] Metasploit installed $nc"
else
echo -e "$white[$green!$white]$red Metasploit not installed$nc"
echo -e "$blue installing ..$nc"
curl https://little-cdn.voxhost.fr/setup-metasploit.sh > msfinstall
chmod 755 msfinstall && ./msfinstall
rm msfinstall
echo -e "$white[$green+$white] Metasploit installed successfully."
fi

if hash curl 2>/dev/null; then
echo -e "$white[$green+$white] Curl installed $nc"
else
echo -e "$white[$green!$white]$red Curl not installed$nc"
echo -e "$blue installing ..$nc"
apt update
apt install curl -y
echo -e "$white[$green+$white] Curl installed successfully."
fi

if hash python3 2>/dev/null; then
echo -e "$white[$green+$white] Python installed $nc"
else
echo -e "$white[$green!$white]$red Python not installed$nc"
echo -e "$blue installing ..$nc"
apt update 
apt install python3 python-is-python3 python3-pip -y
pip install impacket
echo -e "$white[$green+$white] Python installed successfully."
fi

if [ -f $path ]; then 
echo -e "$white[$green+$white] DoublePulsar installed$nc "
else
echo -e "$white[$green!$white]$red DoublePulsar not installed$nc"
echo -e "$blue installing ..$nc"
tmppath=$PWD
git clone https://github.com/ElevenPaths/Eternalblue-Doublepulsar-Metasploit
cp -R -f Eternalblue-Doublepulsar-Metasploit /root
cp /root/Eternalblue-Doublepulsar-Metasploit/eternalblue_doublepulsar.rb /opt/metasploit-framework/embedded/framework/modules/exploits/windows/smb/
rm -rf $tmppath/Eternalblue-Doublepulsar-Metasploit
fi

depend=$(dpkg -s wine32  | grep 'Status' | awk -F':' '/Status: / {print $2}') #Check package wine32 installed
if [ "$depend" = " install ok installed" ]; then
echo -e "$white[$green+$white] Wine installed $nc"
else
echo -e "$white[$green!$white]$red Wine not installed$nc"
echo -e "$blue installing ..$nc"
echo "deb http://http.kali.org/kali kali-rolling main contrib non-free" >> /etc/apt/sources.list
wget https://little-cdn.voxhost.fr/kalikeyring.deb
dpkg -i kalikeyring.deb
rm kalikeyring.deb
dpkg --add-architecture i386
apt update
apt install wine wine32 wine64 winexe windows-binaries -y
winecfg
fi

echo -e "$green"
echo -e "you are ready to launch ispy"
sleep 1
echo -e "launching ispy$nc"
sleep 1
chmod +x ispy
./ispy
